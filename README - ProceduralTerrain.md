Matthieu Ronarc'h - Marius Beaudoin - GP3

R&D : G�n�ration proc�durale de meshes de terrain

Pour notre projet de R&D on s'est plus orient�s vers la partie g�n�ration proc�durale de meshes que sur l'optimisation de meshes.
On a developp� un outil permettant de g�n�rer des chunks de terrain via l'�diteur, de les faire varier grace a l'utilisation
du bruit de perlin mais aussi de les rendre avec un material permettant de personaliser leur aspect. On a aussi impl�ment�
la m�thode 'Marching Cubes' permettant d'�diter le terrain (en creusant ou rajoutant de la matiere) et de la g�n�ration infinie de chunks.

A la fin de la slide (sur le partage et par mail car on etait pas surs) il y a le lien du projet sur GitLab et il est possible
de cloner le repo afin d'acceder au code et au projet en general.

Description des scripts et outils:

ProceduralTerrainManager : manager permettant d'editer un terrain contenant plusieurs chunks, de modifier la precision et la size des chunks
mais aussi la dimension du terrain.

ProceduralTerrain : peut se resumer a un chunk de terrain possedant un mesh g�n�r� grace au manager ci-dessus.

NoiseManager : permet de generer une heightmap utile a la generation de chunks via une liste de bruits de perlins convertis en heightmap
et influenc�s par plusieurs parametres comme la frequence de chaque bruit, mais aussi l'amplitude et la seed. Il y a �galement la possibilit�
de modifier chaque bruit grace a une courbe influencant leur valeurs de ceux ci.

ProceduralTerrainMarchingCubes : h�rite de ProceduralTerrain et rajoute l'impl�mentation de la m�thode 'Marching Cubes' permettant au player
de modifier le terrain en temps reel.

EndlessTerrain : permet de g�n�rer des chunks au fur et a mesure que le player se d�place en play mode. On s'est pench�s sur une version
multithread�e mais on a pas eu le temps de l'impl�menter (code en commentaire).

Grace a ces differents scripts on peut generer un terrain assez complet et complexe (que ce soit avec les noises, ainsi que la methode
'Classique' et les 'Marching Cubes'). L'editeur est assez simple d'utilisation (normalement) et globalement on est arriv�s plus loin
que ce que l'on pensait. C'etait tres interessant de se retrouver a generer des meshes en editor mode parce qu'on a quand meme etes
confront�s a des problematiques assez songulieres (notamment a cause du nombre de vertices dans certains cas).
La generation n'est pas parfaite, le shading non plus, mais on est satisfaits du resultat dans l'ensemble et ca reste fonctionnel et meme
reutilisable pour d'autres projets.

On a pas vraiment constitu� de dossier de recherche mais on a plus d�velopp� pour voir jusqu'ou on pouvait aller, voir ce que l'on peut
faire en terme de generation de terrain.
On a fait aussi un Git commun avec Gregoire et Quentin Mouillade car on pensait pouvoir merge les 3 projets ensemble a la fin pour faire
un terrain avec des arbres et de la meteo mais on a pas vraiment eu le temps non plus. Mais les 3 projets sont dans le meme projet Unity.

On reste disponibles pour r�pondre a toutes les questions :)

Bonne balade dans notre terrain proc�dural avec la Jos�-boule !
