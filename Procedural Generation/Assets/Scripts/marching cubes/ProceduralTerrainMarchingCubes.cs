﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;

namespace MarchingCubes
{
	public class ProceduralTerrainMarchingCubes : ProceduralTerrain
	{
		Vector3[] normals;

		[SerializeField]
		float isoValue = 1;
		[SerializeField]
		bool drawGizmo = false;

		MarchingCubesAlgorithm algorithm = new MarchingCubesAlgorithm();

		List<(int index, float density)> modifications = new List<(int index, float density)>();
		bool areDensitiesDirty => modifications.Count >= 1;

		public void Update()
		{
			if(areDensitiesDirty)
			{
				ApplyDensitiesModifications();
			}
		}

		override protected void ComputeVertices()
		{
			if (heightMap == null)
			{
				if (transform.parent != null) // getting noise offset from parent
				{
					offset.x = transform.position.x - transform.parent.position.x;
					offset.y = transform.position.z - transform.parent.position.z;
				}

				heightMap = noiseManager.GetHeightMap(size, size, offset.x, offset.y, totalWidth, totalLength);

				for (int l = 0; l < size + 1; l++)
				{
					for (int w = 0; w < size + 1; w++)
					{
						amplitude.x = Mathf.Min(amplitude.x, heightMap[l * (size + 1) + w]);
						amplitude.y = Mathf.Max(amplitude.y, heightMap[l * (size + 1) + w]);
					}
				}
			}
			if(densities == null)
				densities = NoiseManager.CalculateDensities(heightMap, size);

			//float[] densities = GetTestDensity(size);
			algorithm.chunkSize = size;
			algorithm.totalSize = totalWidth;
			algorithm.densities = densities;
			algorithm.isoValue = isoValue;
			algorithm.Execute();

			vertices = algorithm.vertices.Slice(0,algorithm.triangleIndex + 1);
			normals = algorithm.normals.Slice(0,algorithm.triangleIndex + 1);
			triangles = algorithm.triangles.Slice(0, algorithm.triangleIndex + 1);
		}

		override protected void SetMeshData()
		{
			mesh = new Mesh();
			/*mesh.SetVertices((Vector3[])null, 0, 0);
			mesh.SetIndices(null, MeshTopology.Triangles, 0);
			mesh.SetUVs(0, (Vector2[])null);*/
			mesh.vertices = vertices;
			mesh.normals = normals;
			mesh.SetTriangles(triangles, 0);
			//mesh.RecalculateNormals();
		}

		public float GetDensity(int x, int y, int z)
		{
			return densities[x * (size + 1) * (size + 1) + y * (size + 1) + z];
		}

		public void ModifyDensity(float density, int x, int y, int z)
		{
			modifications.Add(((x * (size + 1) * (size + 1) + y * (size + 1) + z), density));
			isModified = true;
		}
		public void ModifyDensity(List<(int index, float density)> _modifications)
		{
			modifications.AddRange(_modifications);
			isModified = true;
		}

		private void ApplyDensitiesModifications()
		{
			for(int i = 0; i < modifications.Count; i++)
			{
				densities[modifications[i].index] = modifications[i].density;
			}
			modifications.Clear();

			Generate();
		}

		static float[] GetTestDensity(int size)
		{
			float[] densities = new float[(size + 1) * (size + 1) * (size + 1)];

			for (int x = 0; x < size + 1; x++)
				for (int y = 0; y < size + 1; y++)
					for (int z = 0; z < size + 1; z++)
					{
						if (y < size / 2 && x < size / 2 && z < size / 2)
							densities[x * (size + 1) * (size + 1) + y * (size + 1) + z] = 10f;
						else
							densities[x * (size + 1) * (size + 1) + y * (size + 1) + z] = 0f;
					}


			return densities;
		}

		private void OnDrawGizmosSelected()
		{
			if (drawGizmo)
				for (int x = 0; x < size + 1; x++)
					for (int y = 0; y < size + 1; y++)
						for (int z = 0; z < size + 1; z++)
						{
							float density = densities[x * (size + 1) * (size + 1) + y * (size + 1) + z];
							Vector3 pos = new Vector3(x, y, z) / (float)(size) * totalWidth + transform.position;

							if (density >= isoValue)
							{
								Color c = new Color(density / isoValue, 1, 1);

								Gizmos.color = c;
								Gizmos.DrawWireSphere(pos, 0.3f);
							}
						}
		}
	}
}
