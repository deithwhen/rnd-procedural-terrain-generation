﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(ProceduralTerrainGenerator))]
public class ProceduralTerrainGeneratorEditor : Editor
{
	private bool oldNormalizedGradient = false;
	private float oldUVsMultiplier = 0.0f;

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		ProceduralTerrainGenerator manager = (ProceduralTerrainGenerator)target;

		if (!manager.normalizedGradient)
		{
			manager.gradientAmplitude = EditorGUILayout.Vector2Field("Amplitude", manager.gradientAmplitude);
			manager.UVsMultiplier = EditorGUILayout.FloatField("UVs multiplier", manager.UVsMultiplier);
		}

		if (oldNormalizedGradient != manager.normalizedGradient)
		{
			oldNormalizedGradient = manager.normalizedGradient;
			manager.RefreshTexture();
		}

		if (oldUVsMultiplier != manager.UVsMultiplier)
		{
			oldUVsMultiplier = manager.UVsMultiplier;
			manager.RefreshTexture();
		}


		GUILayout.Label("_________________________________________________");

		if (GUILayout.Button("Apply dimensions"))
		{
			manager.ComputeChunksNumber();
			manager.ComputeChunksPosition();
			manager.RegenerateChunks();
		}

		if (GUILayout.Button("Regenerate chunks"))
		{
			manager.RegenerateChunks();
		}

		if (GUILayout.Button("Add chunk"))
		{
			manager.AddChunk();
			manager.ComputeChunksPosition();
		}

		if (GUILayout.Button("Remove chunk"))
		{
			manager.RemoveChunk();
		}

		GUILayout.Label("_________________________________________________");

		if (GUILayout.Button("Refresh texture"))
		{
			manager.RefreshTexture();
		}

		if (GUILayout.Button("Refresh chunks pool"))
		{
			manager.RefreshPool();
			manager.ComputeChunksNumber();
			manager.ComputeChunksPosition();
			manager.RegenerateChunks();
		}
	}
}
