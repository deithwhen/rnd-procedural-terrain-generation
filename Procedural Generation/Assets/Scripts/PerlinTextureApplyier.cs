﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinTextureApplyier : MonoBehaviour
{
    Noise perlinNoiser ;
    [SerializeField]
    float seed = 0f;
    [SerializeField]
    float zoom = 1f;
    
    // Start is called before the first frame update
    void Start()
    {
        perlinNoiser = new Noise();
        perlinNoiser.frequency = zoom;
        perlinNoiser.seed = seed;
        Texture2D texture = new Texture2D(256, 256);
        perlinNoiser.PrintInTexture(texture);
        byte[] png = texture.EncodeToPNG();
        System.IO.File.WriteAllBytes(Application.dataPath + "/../Images/PerlinNoise.png", png);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
