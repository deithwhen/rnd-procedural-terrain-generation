﻿using UnityEngine;

[System.Serializable]
public class ObjectPool
{
	public GameObject[] objectArray;
	public GameObject prefab;
	public GameObject poolHolder;
	
	// Initialize object pool with given objects and size
	public ObjectPool(GameObject holder, GameObject prefab, int size)
	{
		objectArray = new GameObject[size];

		poolHolder = holder;
		//poolHolder = new GameObject(prefab.name + " Pool");

		for (int i = 0; i < size; ++i)
		{
			objectArray[i] = Object.Instantiate(prefab, poolHolder.transform);
			objectArray[i].SetActive(false);
			objectArray[i].name = prefab.name + "_" + (i + 1);
		}

		//Object.DontDestroyOnLoad(poolHolder);
	}

	// Empty pool by destroying every instantied objects
	public void Clear(bool force = false)
	{
		foreach (GameObject obj in objectArray)
		{
			if (force)
				Object.DestroyImmediate(obj);
			else
				Object.Destroy(obj);
		}

		//Object.Destroy(poolHolder);
	}

	// Return an available object from the pool
	public GameObject Provide()
	{
		foreach (GameObject obj in objectArray)
		{
			if (obj && !obj.activeSelf)
			{
				obj.SetActive(true);
				return obj;
			}
		}

		return null;
	}

	// Recall every object in the pool and set them to not active
	public void Recall()
	{
		foreach (GameObject obj in objectArray)
			if (obj.activeSelf)
				obj.SetActive(false);
	}
}