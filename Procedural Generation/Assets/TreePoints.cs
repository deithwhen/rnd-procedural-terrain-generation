﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreePoints : MonoBehaviour
{
    [SerializeField]
    Vector3[] points;
    [SerializeField]
    int precision;
    [SerializeField]
    float width;

    MeshFilter meshFilter;


    // Update is called once per frame
    void OnValidate()
    {
        if (precision < 3)
            precision = 3;

        meshFilter = GetComponent<MeshFilter>();
        if (meshFilter)
        {
           meshFilter.mesh = MeshTreeGenerator.GenerateTreeMesh(points, precision, width);
           // meshFilter.mesh = MeshTreeGenerator.CreateProceduralCylinder(points[0], points[1], 50, 0.5f);
        }
    }

    private void OnDrawGizmos()
    {
        for(int i = 0; i < points.Length -1; i+=2)
            Gizmos.DrawLine(points[i], points[i + 1]);

        //MeshTreeGenerator.CreateProceduralCylinder(points[0], points[1], 50, 0.5f);
    }
}
