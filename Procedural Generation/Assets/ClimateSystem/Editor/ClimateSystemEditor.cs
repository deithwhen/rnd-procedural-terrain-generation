﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ClimateSystem))]
public class ClimateSystemEditor : Editor
{
	#region Unity scripts
	public override void OnInspectorGUI()
	{
		if (instance == null)
			instance = (ClimateSystem)target;

		base.OnInspectorGUI();

		EditorGUILayout.Separator();

		show_climate_map_preview = EditorGUILayout.Foldout(show_climate_map_preview, "Climate map preview", true);

		if (show_climate_map_preview)
		{
			use_player_position = EditorGUILayout.Toggle("Use player position", use_player_position);

			climate_map_preview_position = use_player_position ? instance.GetPlayer2DPosition() : EditorGUILayout.Vector2Field("Position (at center)", climate_map_preview_position);

			climate_map_preview_texture_size =
				EditorGUILayout.Vector2IntField("Preview size", climate_map_preview_texture_size);

			EditorGUILayout.Separator();

			climate_map_preview_texture =
				instance.GetClimatePreview(climate_map_preview_texture_size, climate_map_preview_position.x, climate_map_preview_position.y);

			float size = use_player_position ? 21.5f : 22.5f;

			if (climate_map_preview_texture)
				EditorGUI.DrawPreviewTexture(new Rect(16, instance.ClimateCount * EditorGUIUtility.singleLineHeight + size * EditorGUIUtility.singleLineHeight, 256, 256), climate_map_preview_texture, null, ScaleMode.ScaleToFit);

			if (use_player_position)
				Repaint();

			EditorGUILayout.Space(256);
		}
	}

	#endregion

	#region Members

	private ClimateSystem instance;

	private Vector2 climate_map_preview_position;

	private Texture2D climate_map_preview_texture;

	private Vector2Int climate_map_preview_texture_size = new Vector2Int(256, 256);

	private bool show_climate_map_preview;

	private bool use_player_position;

	#endregion
}
