﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshTreeGenerator
{
    public static Mesh GenerateTreeMesh(Vector3[] points, int precision, float truncSize)
    {
        Mesh mesh = new Mesh();
        CombineInstance[] combine = new CombineInstance[points.Length / 2];

        for(int i = 0; i < points.Length/2; i++)
        {
            combine[i].mesh = CreateProceduralCylinder(points[i*2], points[i*2+1], precision, truncSize);
        }
        mesh.CombineMeshes(combine,true,false);

        return mesh;
    }

    public static Mesh CreateProceduralCylinder(Vector3 p1, Vector3 p2, int precision, float truncSize)
    {
        Mesh mesh = new Mesh();

        #region Vertices
        Vector3 rotatingVector = Rotate90CW(p2-p1).normalized * truncSize;
        Vector3[] vertices = new Vector3[2 * precision];
        Quaternion rotationPerVertice = Quaternion.AngleAxis(360 / (float)precision, p2-p1);

        for (int i = 0; i < precision; i++)
        {
            vertices[i] = p1 + rotatingVector;
            vertices[i + precision] = p2 + rotatingVector;
            rotatingVector = rotationPerVertice * rotatingVector;
        }
        #endregion

        #region Triangles
        int[] triangles = new int[precision* 6 + (precision - 2) *2 * 3];
        int t = 0;
        for (int i = 0; i < precision ; i++)
        {
            if(i == precision - 1)
            {
                triangles[t++] = i;
                triangles[t++] = precision;
                triangles[t++] = i + precision;

                triangles[t++] = i;
                triangles[t++] = 0;
                triangles[t++] = precision;
                break;
            }

            triangles[t++] = i;
            triangles[t++] = i + precision + 1;
            triangles[t++] = i + precision;

            triangles[t++] = i;
            triangles[t++] = i + 1;
            triangles[t++] = i + precision + 1;
        }
        for(int i = 1; i < precision - 1; i++)
        {
            triangles[t++] = 0;
            triangles[t++] = i + 1;
            triangles[t++] = i;

            triangles[t++] = precision;
            triangles[t++] = precision + i;
            triangles[t++] = precision + i + 1;
        }

        mesh = new Mesh();
        mesh.SetVertices(vertices, 0, vertices.Length);
        mesh.SetIndices(triangles, 0, triangles.Length, MeshTopology.Triangles, 0);
        mesh.RecalculateNormals();

        #endregion

        return mesh;
    }

    static Vector3 Rotate90CW(Vector3 aDir)
    {
        return new Vector3(aDir.y, aDir.z, -aDir.x);
    }
}
